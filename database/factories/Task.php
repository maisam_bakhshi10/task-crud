<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Task\Task;
use Task\User;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
    $title = $faker->sentence;
    return [
        'title' =>  $title,
        'slug'  =>  str_slug($title),
        'description'  => $faker->paragraph,
        'user_id'       =>  function() {
            return User::all()->random();
        }
    ];
});
