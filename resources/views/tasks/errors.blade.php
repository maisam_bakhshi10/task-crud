
@if ($errors->any())
<ul class="border border-danger list-group">
    @foreach ($errors->all() as $error)
            <li class="list-group-item list-group-item-danger"> {{ $error }} </li>
    @endforeach      
</ul>             
@endif