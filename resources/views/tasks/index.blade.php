@extends('layouts.app')

@section('content')
<div class="container">
    <h3 class="text-center">List of the Tasks</h3>
    <ul class="list-group list-group-flush">
    @foreach ($tasks as $task)
        <li class="list-group-item d-flex justify-content-between shadow-sm"><p class="p-0 m-0 flex-grow-1">{{$task->title}}</p>  
            <a href="/task/{{$task->slug}}" class="btn btn-success btn-sm">Detail</a>
        </li>
    @endforeach
</ul>
</div>

@endsection

