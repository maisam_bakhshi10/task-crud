@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-12 mt-3">
            <h1 class="text-center">Create a Task</h1>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-sm-6 mt-3">
            <form action="/task" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group shadow-sm">
                    <input type="text" class="form-control" name="title" id="title" placeholder="Enter Task Title" value="{{old('title')}}">
                </div>
                <div class="form-group shadow-sm">
                <textarea class="form-control" name="description" id="description" rows="10" cols="40"  > {{old('description')}}</textarea>
                </div>
                <button type="submit" class="btn btn-primary">Create Task</button>
            </form>
        </div>
    </div>

    <div class="row justify-content-center">
            <div class="col-sm-6 mt-3">
                @include('tasks.errors')
            </div>
        </div>
</div>
@endsection