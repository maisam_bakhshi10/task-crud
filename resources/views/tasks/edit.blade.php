@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-12 mt-3">
            <h1 class="text-center">Update a task</h1>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-sm-6 mt-3">
            <form action="/task/{{ $task->slug }}" method="POST">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <input type="text" class="form-control" name="title" id="title" placeholder="Enter Task Title" value="{{ $task->title }}">
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="description" id="description" rows="11" cols="40"  >{{ $task->description }}</textarea>
                </div>
                <button type="submit" class="btn btn-primary">Update Task</button>
            </form>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-sm-6 mt-3">
            @include('tasks.errors')
        </div>
    </div>

@endsection