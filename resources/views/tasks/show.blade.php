@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card shadow-sm">
                <div class="card-header text-center">
                    <h3> {{$task->title }}</h3>
                </div>
                <div class="card-body">
                    <p class="card-text">{{$task->description}}</p>
                    <div class="row">
                        <div class="col-xs-4 mr-3 ml-3">
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">
                                Delete
                            </button>
                        </div>
                        <div class="col-xs-4">
                            <a class="btn btn-info mb-3" href="/task/{{ $task->slug }}/edit">Edit Post</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Delete form -->
<form action="/task/{{$task->slug}}" method="POST">
    @csrf
    @method('DELETE')
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="deleteModalLabel">Delete Task</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <h4>Are you Sure?</h4>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-lg" data-dismiss="modal">No</button>
            <button type="submit" class="btn btn-danger btn-lg">Yes</button>
            </div>
        </div>
        </div>
    </div>
</form>



@endsection