@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col align-self-center">
            <div class="first-section text-center m-3">
                <h1>tasks Manager</h1>
            </div>

            <div class="instructions shadow rounded m-5">
                <ul class="list-group">
                    <li class="list-group-item active text-center"><h2>How to create a task</h2></li>
                    <ol class="list-group-item">Click on the <strong>Log In</strong> - Enter your login Details</ol>
                    <li class="list-group-item">Click on the <strong>Create</strong> from the menu</li>
                    <li class="list-group-item">Click on the <strong>Detail</strong> to read full task</li>
                    <li class="list-group-item">Click on <strong>Edit</strong> if you want to edit the task </li>
                </ul>
            </div>
            
        </div>
    </div>
</div>
@endsection