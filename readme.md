
# Getting started

## Installation


Clone the repository

    git clone git clone https://bitbucket.org/maisam_bakhshi10/task-crud.git

Switch to the repo folder

    cd task-crud

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate


Run the database migrations with seed

    php artisan migrate --seed

Start the local development server

Run npm compiler

    npm run watch

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000